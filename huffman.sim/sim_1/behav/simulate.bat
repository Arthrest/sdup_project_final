@echo off
set xv_path=D:\\Xilinx\\Vivado\\2016.4\\bin
call %xv_path%/xsim huffman_codec_tb_behav -key {Behavioral:sim_1:Functional:huffman_codec_tb} -tclbatch huffman_codec_tb.tcl -view D:/Studia magisterskie/SDUP_Projekt/huff_encoder_tb_behav.wcfg -view D:/Studia magisterskie/SDUP_Projekt/huffman_tb_behav.wcfg -log simulate.log
if "%errorlevel%"=="0" goto SUCCESS
if "%errorlevel%"=="1" goto END
:END
exit 1
:SUCCESS
exit 0
