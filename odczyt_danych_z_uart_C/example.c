#include<stdio.h>
#include<stdint.h>

// Test funkcji czytajacej z konsoli znaki, az do napotkania znaku \n i konwertujacej znaki ASCII do liczb dziesietnych.

size_t readStreamOfNumbers(char* data){
    if(data != NULL){
        int i = 0;
        do{
            data[i] = getchar();
            i++;
        } while(((char)data[i-1]) != '\n');
        return i-1;
    }
    return 0;
}

int main()
{
    uint8_t stream[256];
    printf("Napisz cos: \n");
    size_t streamLength = readStreamOfNumbers(stream);
    
    for(int j=0; j<streamLength; j++){
        printf("Przeczytano znak %c o numerze %i.\n",stream[j], stream[j]);
    }
    return 0;
}