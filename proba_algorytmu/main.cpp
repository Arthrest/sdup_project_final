/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, PHP, Ruby,
C#, VB, Perl, Swift, Prolog, Javascript, Pascal, HTML, CSS, JS
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <iostream>
#include <cstdlib>
#include <bitset>
#include <string>
#include <cstring>
#include <cmath>

using namespace std;
/// sortowanie bombelkowe xD
void sort (int length, int *symbols, int *probs, int* link){
  int i = 0;
  while (i < length){
      for (int j = 0; j < length - 1; j++){
          if (probs[j] >= probs[j + 1] && probs[j] > 0){
              int temp = probs[j];
              int temp2 = symbols[j];
              int temp3 = link[j];
              probs[j] = probs[j + 1];
              symbols[j] = symbols[j + 1];
              link[j] = link[j + 1];
              probs[j + 1] = temp;
              symbols[j + 1] = temp2;
              link[j + 1] = temp3;
            }
        }
      i++;
    }
}

int search(int* tab, int length, int value){
    int it = 0;
    for(int i = 0; i < length; i++){
        if(value != tab[i]){
            it++;
        }
        else{
            break;
        }
    }
    return it;
}




void decoder(string dataStream, int *codes, int numberOfSymbols, int* codesLengths, int* symbols){
    cout << "Dekoder" << endl;
    //tymczasowa konwersja, w rzeczywistosci bity nie b�d� elementami stringa
    int streamLength = 0;
    while(dataStream[streamLength] != '\0'){
        streamLength++;
    }
    int bits[streamLength];
    for(int i=0; i<streamLength; i++){
        if(dataStream[i] == '0'){
            bits[i] = 0;
        } else{
            bits[i] = 1;
        }
    }
    ///////////////////
    char decodedCharacters[100];
    int decodedCharacterIndex = 0;
    cout << "Kody ASCII: ";
    int characterLength = 1;
    // petla przesuwajaca sie przez kolejne bity wchodzacego strumienia
    for(int bitPosition = 0; bitPosition<streamLength; bitPosition++){
    		// zmienne lokalne stworzone po to, by nie modyfikowac iteratorow petli for
            int codeIndex = characterLength;
            int bitIndex = bitPosition;
            int codeValue = 0;
            // petla skladajaca wartosc liczby z bitow wejsciowych mnozac je przez potegi dwojek np:
            // informacje po potedze dwojki w danej iteracji przechowuje characterLength (dlugosc juz odczytanego kodu)
            // 101 => 2^2* 1 + 2^1 * 0 + 2^0 * 1
            while(codeIndex != 0){
                codeValue += ((int)pow(2,codeIndex-1))*bits[bitIndex];
                codeIndex--;
                bitIndex--;
            }
        // petla przesuwajaca sie przez kolejne kody w tablicy, szukajac pasujacego do bitow wejsciowych
        for(int codePosition = 0; codePosition<numberOfSymbols; codePosition++){
          //  cout << "Wartosc " << codeValue << endl;
          //  cout << "and " << (codes[codePosition]&((int)(pow(2,characterLength))-1)) << endl;
        	// sprawdzenie czy dany kod pokrywa sie z aktualnie odczytanym ciagiem bitow
        	// sprawdzenie odbywa sie na podstawie operacji AND danego kodu z iloscia jedynek odpowiadajaca
        	// ilosci odczytanych bitow, po to by wyzerowac reszte. Np. na chwile obecna mamy 3 bity i ich szukamy,
        	// reszta nas nie obchodzi, wiec jest zerowana
            if((codes[codePosition]&((int)(pow(2,characterLength))-1)) == codeValue){
               // cout << "Pozycja " << codePosition << endl;
            	// sprawdzenie czy znaleziony kod ma dlugosc zgodna z iloscia bitow
            	// jesli kod ma wiecej znakow, dobiera sie kolejny bit ze strumienia
                if(codesLengths[codePosition] > characterLength){
                    characterLength++;
                } else{
                	// jesli kod ma tyle znakow co jest pobranych bitow, przywraca sie zmienne do stanu poczatkowego
                	// i zapisuje sie symbol odpowiadajacy kodowi do tablicy zdekodowanych znakow
                    characterLength = 1;
                    cout << " " << symbols[codePosition];
                    decodedCharacters[decodedCharacterIndex] = (char)symbols[codePosition];
                    decodedCharacterIndex++;
                }
                break;
            }
        }
    }
    cout << endl << "Zdekodowany ciag: ";
    for(int x=0; x<decodedCharacterIndex; x++){
        cout << decodedCharacters[x];
    }

}
//POLSKI
//{' ', ',', '.', ';', A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, R, S, T, U, W, Y, Z,
// a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,r,s,t,u,w,y,z}
//{32, 44, 46, 59, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 82, 83, 84, 85, 87, 89, 90,
// 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 114, 115, 116, 117, 119, 121, 122}

//

int main (){
/// zmienne wejsciowe do programu - te zmienne i tablice musimy zaladowac z innego komponentu

    // dane dla j�zyka polskiego

    //int symbol_tab [27] = {32, 44, 46, 59, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 82, 83, 84, 85, 87, 89, 90};
    //int prob_tab[27] = {172,9,9,5,80,13,38,30,69,1,10,10,70,19,27,31,24,47,71,24,35,38,24,18,36,32,58};
    //int length = 27; // poki co nie mozna dac tu wiekszej wartosci, bo while sie zwiesza -> powinno byc 50
    int symbol_tab [50] = {32, 44, 46, 59, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 82, 83, 84, 85, 87, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 114, 115, 116, 117, 119, 121, 122};
    int prob_tab[50] = {1720, 90, 90, 50, 400, 65, 190, 150, 345, 5, 50, 50, 350, 95, 135, 155, 120, 235, 355, 120, 175, 190, 120, 90, 180, 160, 290, 400, 65, 190, 150, 345, 5, 50, 50, 350, 95, 135, 155, 120, 235, 355, 120, 175, 190, 120, 90, 180, 160, 290};
    int length = 50;

    /// dane wejsciowe: 12,14,15,12,13,13,14,14,12,16
    // int symbol_tab [5] = {12, 13, 14, 15, 16};
    // int prob_tab[5] = {3, 2, 3 , 1, 1};
    // int length = 5;

/// zmienne ktore tworzymy w obiekcie:
    int tab_length = length * 2 - 1;
    //cout << tab_length << endl;

    int Id[tab_length];
    int P[tab_length];
    int Link[tab_length];
    int code_list[length];
    int code_length[length];
/// inicjalizacja tablic
/// potrzebujemy:
/// na wej�ciu:
/// 1. tablice symboli: symbol_tab[ilosc_symboli]
/// 2. tablice prawdopodobienstw: prob_tab[ilosc_symboli]
/// 3. tablice symboli wraz z dodatkowym miejsce na przetwarzanie w przechodzeniu drzewa : Id[ilosc_symboli*2-1]
/// 4. tablice prawdopodobienstw wraz z dodatkowym miejscem na przetwarzanie w przechodzeniu drzewa : P[ilosc_symboli*2-1]
/// 5. tablice par potrzebna do przetwarzania w przechodzeniu drzewa : Link[ilosc_symboli*2-1]
/// 6. tablice na kody : code_list[ilosc_symboli]
/// 7. tablice na dlugosc kodow: code_length[ilosc_symboli]
/// Wszystkie tablice inicjalizujemy zerami albo stanami nieustalonymi aby pozniej w obliczeniach nie bylo problemow
    for(int i = 0; i < tab_length; i++){
        Id[i] = 0;
        P[i] = 0;
        Link[i] = 0;
        code_list[i] = 0;
        code_length[i] = 0;
    }
    cout << "symbols:    "; for(int i = 0; i < length; i++) { cout << " | " << symbol_tab[i] << " | "; } cout << endl;
    cout << "pobability: "; for(int i = 0; i < length; i++) { cout << " |  " << prob_tab[i] << " | "; } cout << endl << endl;
/// ladujemy na miejsca 1 - N symbole i prawdopodobienstwa w do tablic Id oraz P
    for(int i = 0; i < length; i++){
        Id[i] = symbol_tab[i];
        P[i] = prob_tab[i];
    }
/// inicjalizujemy iteratory potrzebne do budowania drzewa
    int it = 0;
    int counter = 0;
    //cout << "Id:    "; for(int i = 0; i < tab_length; i++) { cout << " | " << Id[i]   << " | "; } cout << endl;
    //cout << "P:     "; for(int i = 0; i < tab_length; i++) { cout << " | " << P[i]    << " | "; } cout << endl;
    //cout << "Link:  "; for(int i = 0; i < tab_length; i++) { cout << " | " << Link[i] << "  | "; } cout << endl << endl;
/// tworzenie drzewa
/// 1. posortuj tablice
/// 2. znajdz dwa symbole z najmniejszymi prawdopodobinstwami - pierwszy i drugi element z tablicy
/// 3. w tablicy Id na kolejnym pustym miejscu [length+id] wstaw nowy indeks - moze byc dowolna liczba, nieostotne jaka
/// 4. w tablic P na tym samym miejsciu [length+id] wstaw sume prawdopodbienstw dwoch znalezionych najmniejszych prawdopodbienstw
/// 5. w tablicy P na miejscu pierwszego z znalezionych najmniejszych prawdopodobienstw [counter] wstaw 0 a na drugim miejscu [counter+1] wstaw 1 (w przypadku tego programu jest to -1 - sprawa do roziwazania)
/// 6. w tablicy Link na miejscach znalezionych najmniejszych prawdopodbienstw Link[counter] & Link[counter+1] wartosc nowo utworzonego Id[length+id]
/// powtarzaj kroki 1- 6 zwiekszajac iterator tablicy o 1 oraz iterator counter o 2 az przeleci liczba dlugosci symboli - 1
/// osiagniete tablice powinny miec przykladowy format:
/// Id:   | A | B | C | D | E | 5 | 6 | 7 | 8  |
/// P:    | 0 | 1 | 0 | 1 | 0 | 1 | 0 | 1 | 10 |
/// Link  | 5 | 5 | 6 | 6 | 7 | 7 | 8 | 8 | 0  |
    for(volatile int c = 0; c < length-1; c++){
    sort(length+it, Id, P, Link);               /// (1)
    //cout << "Id:    "; for(int i = 0; i < tab_length; i++) { cout << "  |  " << Id[i] << " | "; } cout << endl;
    //cout << "P:     "; for(int i = 0; i < tab_length; i++) { cout << "  |  " << P[i] << " | "; } cout << endl;
    //cout << "Link:  "; for(int i = 0; i < tab_length; i++) { cout << "  |   " << Link[i] << "  |  "; } cout << endl << endl;
    Id[length+it] = length*10+it+1;                  /// (3)
    P[length+it] = P[counter] + P[counter+1];   /// (4)
    P[counter] = 0;                             /// (5)
    P[counter+1] = 1;                          /// (5)
    Link[counter] = Id[length+it];              /// (6)
    Link[counter+1] = Id[length+it];            /// (6)
    it++;
    counter = counter + 2;
    }
    cout << "Id:    "; for(int i = 0; i < tab_length; i++) { cout << " | " << Id[i]   << " | "; } cout << endl;
    cout << "P:     "; for(int i = 0; i < tab_length; i++) { cout << " | " << P[i]    << "  |"; } cout << endl;
    cout << "Link:  "; for(int i = 0; i < tab_length; i++) { cout << " | " << Link[i] << "  | "; } cout << endl << endl;

/// przechodzenie drzewa
/// algorytm
/// potrzebujemy dwa iteratory
/// jeden do iteracji kodow - za jeden przebieg petli da sie zakodowac jeden symbol
/// drugi iterator do przechodzenia tablicy It
/// 1. zapisz do zmiennej tymczasowej symbol wartosc kolejnego symbolu z tablicy symbolow
/// 2. zapisz do zmiennej current_symbol wartosc obecnego symbolu zmienna ta bedzie wykorzystywana do kolejnych iteracji
/// 3. oblicz pozycje wybranego symbolu w tablicy Id - algorytm w funkcji na gorze
/// 4. iezeli P[obliczonej pozycji] jest rowny 0 przesun bitowo tablice kodow o 1 (mnozymy razy 2)i dodaj 0
/// 5. jezeli P[obliczonej pozycji] jest rowny 1(tutaj chwilowo -1) przesun bitowo tablice kodow o 1(mnozymy razy 2) i dodaj 1
/// 6. zwieksz dlugosc danego symbolu o 1
/// 7. zamien obecny symbol na ten z tablicy Link z obliczonej pozycji
/// 8. jesli uzyskany nowy symbol nie jest rowny ostatniemu z drzewa z tablicy Id[ilosc_symboli*2-1] powtorz kroki 3 - 7
/// 9. zwieksz iterator symbolowy dopoki nie przeleci wszystkich symboli - ilosc_symboli

    int code_it = 0;
    int symbol_it = 0;
    int symbol = 0;
    int pozycja = 0;
    int current_symbol = 0;
    int last_symbol = Id[tab_length-1];
    //cout << last_symbol << endl;
    for(volatile int i=0; i < length; i++){
        symbol = symbol_tab[symbol_it];                             /// (1)
        current_symbol = symbol;                                    /// (2)
        code_list[symbol_it] = 0;
        cout << "symbol: " << symbol << endl;
        cout << "current_symbol: " << current_symbol << endl;
        while(current_symbol != last_symbol){                       /// (8)
            pozycja = search(Id, tab_length, current_symbol);       /// (3)
            cout << "current symbol: " << current_symbol << " current position: " << pozycja << " current sign: " << P[pozycja] << " current it: " << symbol_it;
            if(P[pozycja] == 0){                                    /// (4)
                code_list[symbol_it]  *= 2;                         /// (4)
            }else if(P[pozycja] == 1){                             /// (5)
                code_list[symbol_it] *= 2;                          /// (5)
                code_list[symbol_it] += 1;                          /// (5)
            }
            code_length[symbol_it] = code_length[symbol_it] + 1;    /// (6)
            current_symbol = Link[pozycja];                         /// (7)
            cout << " next symbol: " << current_symbol << " code: " << bitset<8>(code_list[symbol_it]) << endl;

        }
         symbol_it++;                                                /// (9)
     }
    cout << "utworzone kody: " << endl;
    for(int i = 0; i < length; i++) { cout << "symbol: " << symbol_tab[i] << "  code: " << code_list[i] << "  code length: " << code_length[i] << endl; }

    cout << "sybmole" << endl;
    for(int i = 0; i < length; i++) { cout << symbol_tab[i] << endl; }
    cout << "kody" << endl;
    for(int i = 0; i < length; i++) { cout << code_list[i] << endl; }
    cout << "dlugosci" << endl;
    for(int i = 0; i < length; i++) { cout << code_length[i] << endl; }


// /// kodowanie ciagu:
//     // int data_in[10] = {12,14,15,12,13,13,14,14,12,16};
//     // int data_in_length = 10;

    int data_in[100];
    int readIterator = 0;

    cout << "Wpisz ciag znakow do zakodowania: " << endl;

    do{
            char c;
            cin >> c;
        data_in[readIterator] = c;
        readIterator++;
    } while(((char)data_in[readIterator-1]) != '\n');

    int data_in_length = readIterator;
    int data_out[data_in_length];
    string data_out_char;

    for(int i = 0; i < data_in_length; i++){
        pozycja = search(symbol_tab, length, data_in[i]);
        int l = 1;
        for(int j = 0; j <code_length[pozycja]; j++){
            if((code_list[pozycja] & l) != 0){
                data_out_char.append("1");
            }else{
                data_out_char.append("0");
            }
            l *= 2;
        }
        //cout << symbol_tab[pozycja] << endl;
        //cout << data_out_char << endl;
        data_out[i] = code_list[pozycja];
    }
    cout << "data in: "; for(int i = 0; i < data_in_length; i++){ cout << data_in[i] << "  ";} cout << endl;
    cout << "data out: "; for(int i = 0; i < data_in_length; i++){ cout << data_out[i] << "  ";} cout << endl;
    cout << "data out binary codes: " << data_out_char << endl;


// // /// dekodowanie ciagu:
// // /// co bedzie potrzebne:
// // /// drzewo utworzone w ramach kodowania czyli tablice

     decoder(data_out_char, code_list, length, code_length, symbol_tab);

  return 0;
}
