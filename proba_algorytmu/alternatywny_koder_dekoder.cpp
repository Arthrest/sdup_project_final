#include <iostream>
#include <cstdlib>
#include <bitset>
#include <string>
#include <cstring>
#include <cmath>

using namespace std;

void sort (int length, int *symbols, int *probs){
  int i = 0;
  while (i < length){
      for (int j = 0; j < length - 1; j++){
          if (probs[j] <= probs[j + 1] && probs[j] > 0){
              int temp = probs[j];
              int temp2 = symbols[j];
              probs[j] = probs[j + 1];
              symbols[j] = symbols[j + 1];
              probs[j + 1] = temp;
              symbols[j + 1] = temp2;
            }
        }
      i++;
    }
}

int search(int* tab, int length, int value){
    int it = 0;
    for(int i = 0; i < length; i++){
        if(value != tab[i]){
            it++;
        }
        else{
            break;
        }
    }
    return it;
}

void decoder(string dataStream, uint64_t *codes, int numberOfSymbols, int* codesLengths, int* symbols){
    cout << "Dekoder" << endl;
    //tymczasowa konwersja, w rzeczywistosci bity nie b�d� elementami stringa
    int streamLength = 0;
    while(dataStream[streamLength] != '\0'){
        streamLength++;
    }
    int bits[streamLength];
    for(int i=0; i<streamLength; i++){
        if(dataStream[i] == '0'){
            bits[i] = 0;
        } else{
            bits[i] = 1;
        }
    }
    ///////////////////
    char decodedCharacters[100];
    int decodedCharacterIndex = 0;
    cout << "Kody ASCII: ";
    int characterLength = 1;
    // petla przesuwajaca sie przez kolejne bity wchodzacego strumienia
    for(int bitPosition = 0; bitPosition<streamLength; bitPosition++){
    		// zmienne lokalne stworzone po to, by nie modyfikowac iteratorow petli for
            int codeIndex = 0;
            int bitIndex = bitPosition;
            uint64_t codeValue = 0;
            // petla skladajaca wartosc liczby z bitow wejsciowych mnozac je przez potegi dwojek np:
            // informacje po potedze dwojki w danej iteracji przechowuje characterLength (dlugosc juz odczytanego kodu)
            // 101 => 2^2* 1 + 2^1 * 0 + 2^0 * 1
            while(codeIndex != characterLength){
                codeValue += ((uint64_t)pow(2,codeIndex))*bits[bitIndex];
                codeIndex++;
                bitIndex--;
            }
        // petla przesuwajaca sie przez kolejne kody w tablicy, szukajac pasujacego do bitow wejsciowych
        for(int codePosition = 0; codePosition<numberOfSymbols; codePosition++){
            //cout << "Wartosc " << codeValue << endl;
            //cout << "and " << (codes[codePosition]&((int)(pow(2,characterLength))-1)) << endl;
        	// sprawdzenie czy dany kod pokrywa sie z aktualnie odczytanym ciagiem bitow
        	// sprawdzenie odbywa sie na podstawie operacji AND danego kodu z iloscia jedynek odpowiadajaca
        	// ilosci odczytanych bitow, po to by wyzerowac reszte. Np. na chwile obecna mamy 3 bity i ich szukamy,
        	// reszta nas nie obchodzi, wiec jest zerowana
            if((codes[codePosition]&((uint64_t)(pow(2,characterLength))-1)) == codeValue){
               // cout << "Pozycja " << codePosition << endl;
            	// sprawdzenie czy znaleziony kod ma dlugosc zgodna z iloscia bitow
            	// jesli kod ma wiecej znakow, dobiera sie kolejny bit ze strumienia
                if(codesLengths[codePosition] > characterLength){
                    characterLength++;
                } else{
                	// jesli kod ma tyle znakow co jest pobranych bitow, przywraca sie zmienne do stanu poczatkowego
                	// i zapisuje sie symbol odpowiadajacy kodowi do tablicy zdekodowanych znakow
                    characterLength = 1;
                    cout << " " << symbols[codePosition];
                    decodedCharacters[decodedCharacterIndex] = (char)symbols[codePosition];
                    decodedCharacterIndex++;
                }
                break;
            }
        }
    }
    cout << endl << "Zdekodowany ciag: ";
    for(int x=0; x<decodedCharacterIndex; x++){
        cout << decodedCharacters[x];
    }
    cout << endl;
}
//POLSKI
//{' ', ',', '.', ';', A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, R, S, T, U, W, Y, Z,
// a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,r,s,t,u,w,y,z}
//{32, 44, 46, 59, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 82, 83, 84, 85, 87, 89, 90,
// 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 114, 115, 116, 117, 119, 121, 122}

//

int main (){
	/// zmienne wejsciowe do programu - te zmienne i tablice musimy zaladowac z innego komponentu

    // dane dla j�zyka polskiego
	int l_elementow = 50;
	int wartosc_elementow[l_elementow] = {32, 44, 46, 59, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 82, 83, 84, 85, 87, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 114, 115, 116, 117, 119, 121, 122};
	int prawdopodobienstwa[l_elementow] = {1720, 90, 90, 50, 400, 65, 190, 150, 345, 5, 50, 50, 350, 95, 135, 155, 120, 235, 355, 120, 175, 190, 120, 90, 180, 160, 290, 400, 65, 190, 150, 345, 5, 50, 50, 350, 95, 135, 155, 120, 235, 355, 120, 175, 190, 120, 90, 180, 160, 290};
	uint64_t tablica_kodow[l_elementow];
	int dlugosci_kodow[l_elementow];
	int symbole_kodow[l_elementow];

	// sortowanie elementow wzgledem prawdopodobienstwa
	sort(l_elementow, wartosc_elementow, prawdopodobienstwa);

	// tworzenie tablicy kodow
	for(int i=0; i<l_elementow; i++){
		if(i==0){
			tablica_kodow[i] = 0;
			dlugosci_kodow[i] = 1;
			symbole_kodow[i] = wartosc_elementow[i];
		} else if(i==l_elementow - 1){
			tablica_kodow[i] = tablica_kodow[i-1] + 1;
			dlugosci_kodow[i] = i;
			symbole_kodow[i] = wartosc_elementow[i];
		}
		else{
			tablica_kodow[i] = tablica_kodow[i-1] + pow(2,i);
			dlugosci_kodow[i] = i+1;
			symbole_kodow[i] = wartosc_elementow[i];
		}
		cout << "Symbol: " << symbole_kodow[i] << " dlugosc_kodu: " << dlugosci_kodow[i] << " kod: " << bitset<64>(tablica_kodow[i]) << endl;
	}

    int data_in[100];
    int readIterator = 0;
    int pozycja = 0;
    cout << "Wpisz ciag znakow do zakodowania: " << endl;

	// pobieranie strumienia znakow
    do{
        data_in[readIterator] = getchar();
        readIterator++;
    } while(((char)data_in[readIterator-1]) != '\n');

	// kodowanie
    int data_in_length = readIterator-1;
    uint64_t data_out[data_in_length];
    string data_out_char;
    cout << data_in_length << endl;
    for(int i = 0; i < data_in_length; i++){
        pozycja = search(symbole_kodow, l_elementow, data_in[i]);
        uint64_t l = pow(2,dlugosci_kodow[pozycja]-1);
        for(int j = 0; j <dlugosci_kodow[pozycja]; j++){
            if((tablica_kodow[pozycja] & l) != 0){
            	data_out_char.append("1");
            }else{
            	data_out_char.append("0");
            }
            l /= 2;
        }
        data_out[i] = tablica_kodow[pozycja];
    }
    cout << "data in: "; for(int i = 0; i < data_in_length; i++){ cout << data_in[i] << "  ";} cout << endl;
    cout << "data out: "; for(int i = 0; i < data_in_length; i++){ cout << data_out[i] << "  ";} cout << endl;
    cout << "data out binary codes: " << data_out_char << endl;

	decoder(data_out_char, tablica_kodow, l_elementow, dlugosci_kodow, symbole_kodow);

	return 0;
}
