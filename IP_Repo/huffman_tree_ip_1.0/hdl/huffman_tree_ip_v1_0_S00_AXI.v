
`timescale 1 ns / 1 ps

	module huffman_tree_ip_v1_0_S00_AXI #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line

		// Width of S_AXI data bus
		parameter integer C_S_AXI_DATA_WIDTH	= 32,
		// Width of S_AXI address bus
		parameter integer C_S_AXI_ADDR_WIDTH	= 4
	)
	(
		// Users to add ports here

		// User ports ends
		// Do not modify the ports beyond this line

		// Global Clock Signal
		input wire  S_AXI_ACLK,
		// Global Reset Signal. This Signal is Active LOW
		input wire  S_AXI_ARESETN,
		// Write address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR,
		// Write channel Protection type. This signal indicates the
    		// privilege and security level of the transaction, and whether
    		// the transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_AWPROT,
		// Write address valid. This signal indicates that the master signaling
    		// valid write address and control information.
		input wire  S_AXI_AWVALID,
		// Write address ready. This signal indicates that the slave is ready
    		// to accept an address and associated control signals.
		output wire  S_AXI_AWREADY,
		// Write data (issued by master, acceped by Slave) 
		input wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA,
		// Write strobes. This signal indicates which byte lanes hold
    		// valid data. There is one write strobe bit for each eight
    		// bits of the write data bus.    
		input wire [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB,
		// Write valid. This signal indicates that valid write
    		// data and strobes are available.
		input wire  S_AXI_WVALID,
		// Write ready. This signal indicates that the slave
    		// can accept the write data.
		output wire  S_AXI_WREADY,
		// Write response. This signal indicates the status
    		// of the write transaction.
		output wire [1 : 0] S_AXI_BRESP,
		// Write response valid. This signal indicates that the channel
    		// is signaling a valid write response.
		output wire  S_AXI_BVALID,
		// Response ready. This signal indicates that the master
    		// can accept a write response.
		input wire  S_AXI_BREADY,
		// Read address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR,
		// Protection type. This signal indicates the privilege
    		// and security level of the transaction, and whether the
    		// transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_ARPROT,
		// Read address valid. This signal indicates that the channel
    		// is signaling valid read address and control information.
		input wire  S_AXI_ARVALID,
		// Read address ready. This signal indicates that the slave is
    		// ready to accept an address and associated control signals.
		output wire  S_AXI_ARREADY,
		// Read data (issued by slave)
		output wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA,
		// Read response. This signal indicates the status of the
    		// read transfer.
		output wire [1 : 0] S_AXI_RRESP,
		// Read valid. This signal indicates that the channel is
    		// signaling the required read data.
		output wire  S_AXI_RVALID,
		// Read ready. This signal indicates that the master can
    		// accept the read data and response information.
		input wire  S_AXI_RREADY
	);

	// AXI4LITE signals
	reg [C_S_AXI_ADDR_WIDTH-1 : 0] 	axi_awaddr;
	reg  	axi_awready;
	reg  	axi_wready;
	reg [1 : 0] 	axi_bresp;
	reg  	axi_bvalid;
	reg [C_S_AXI_ADDR_WIDTH-1 : 0] 	axi_araddr;
	reg  	axi_arready;
	reg [C_S_AXI_DATA_WIDTH-1 : 0] 	axi_rdata;
	reg [1 : 0] 	axi_rresp;
	reg  	axi_rvalid;

	// Example-specific design signals
	// local parameter for addressing 32 bit / 64 bit C_S_AXI_DATA_WIDTH
	// ADDR_LSB is used for addressing 32/64 bit registers/memories
	// ADDR_LSB = 2 for 32 bits (n downto 2)
	// ADDR_LSB = 3 for 64 bits (n downto 3)
	localparam integer ADDR_LSB = (C_S_AXI_DATA_WIDTH/32) + 1;
	localparam integer OPT_MEM_ADDR_BITS = 1;
	//----------------------------------------------
	//-- Signals for user logic register space example
	//------------------------------------------------
	//-- Number of Slave Registers 4
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg0;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg1;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg2;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg3;
	wire	 slv_reg_rden;
	wire	 slv_reg_wren;
	reg [C_S_AXI_DATA_WIDTH-1:0]	 reg_data_out;
	integer	 byte_index;

	// I/O Connections assignments

	assign S_AXI_AWREADY	= axi_awready;
	assign S_AXI_WREADY	= axi_wready;
	assign S_AXI_BRESP	= axi_bresp;
	assign S_AXI_BVALID	= axi_bvalid;
	assign S_AXI_ARREADY	= axi_arready;
	assign S_AXI_RDATA	= axi_rdata;
	assign S_AXI_RRESP	= axi_rresp;
	assign S_AXI_RVALID	= axi_rvalid;
	// Implement axi_awready generation
	// axi_awready is asserted for one S_AXI_ACLK clock cycle when both
	// S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_awready is
	// de-asserted when reset is low.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_awready <= 1'b0;
	    end 
	  else
	    begin    
	      if (~axi_awready && S_AXI_AWVALID && S_AXI_WVALID)
	        begin
	          // slave is ready to accept write address when 
	          // there is a valid write address and write data
	          // on the write address and data bus. This design 
	          // expects no outstanding transactions. 
	          axi_awready <= 1'b1;
	        end
	      else           
	        begin
	          axi_awready <= 1'b0;
	        end
	    end 
	end       

	// Implement axi_awaddr latching
	// This process is used to latch the address when both 
	// S_AXI_AWVALID and S_AXI_WVALID are valid. 

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_awaddr <= 0;
	    end 
	  else
	    begin    
	      if (~axi_awready && S_AXI_AWVALID && S_AXI_WVALID)
	        begin
	          // Write Address latching 
	          axi_awaddr <= S_AXI_AWADDR;
	        end
	    end 
	end       

	// Implement axi_wready generation
	// axi_wready is asserted for one S_AXI_ACLK clock cycle when both
	// S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_wready is 
	// de-asserted when reset is low. 

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_wready <= 1'b0;
	    end 
	  else
	    begin    
	      if (~axi_wready && S_AXI_WVALID && S_AXI_AWVALID)
	        begin
	          // slave is ready to accept write data when 
	          // there is a valid write address and write data
	          // on the write address and data bus. This design 
	          // expects no outstanding transactions. 
	          axi_wready <= 1'b1;
	        end
	      else
	        begin
	          axi_wready <= 1'b0;
	        end
	    end 
	end       

	// Implement memory mapped register select and write logic generation
	// The write data is accepted and written to memory mapped registers when
	// axi_awready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted. Write strobes are used to
	// select byte enables of slave registers while writing.
	// These registers are cleared when reset (active low) is applied.
	// Slave register write enable is asserted when valid address and data are available
	// and the slave is ready to accept the write address and write data.
	assign slv_reg_wren = axi_wready && S_AXI_WVALID && axi_awready && S_AXI_AWVALID;

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      slv_reg0 <= 0;
	      slv_reg1 <= 0;
	      //slv_reg2 <= 0;
	      //slv_reg3 <= 0;
	    end 
	  else begin
	    if (slv_reg_wren)
	      begin
	        case ( axi_awaddr[ADDR_LSB+OPT_MEM_ADDR_BITS:ADDR_LSB] )
	          2'h0:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 0
	                slv_reg0[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          2'h1:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 1
	                slv_reg1[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          2'h2:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 2
	                //slv_reg2[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          2'h3:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 3
	                //slv_reg3[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          default : begin
	                      slv_reg0 <= slv_reg0;
	                      slv_reg1 <= slv_reg1;
	                     // slv_reg2 <= slv_reg2;
	                     // slv_reg3 <= slv_reg3;
	                    end
	        endcase
	      end
	  end
	end    

	// Implement write response logic generation
	// The write response and response valid signals are asserted by the slave 
	// when axi_wready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted.  
	// This marks the acceptance of address and indicates the status of 
	// write transaction.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_bvalid  <= 0;
	      axi_bresp   <= 2'b0;
	    end 
	  else
	    begin    
	      if (axi_awready && S_AXI_AWVALID && ~axi_bvalid && axi_wready && S_AXI_WVALID)
	        begin
	          // indicates a valid write response is available
	          axi_bvalid <= 1'b1;
	          axi_bresp  <= 2'b0; // 'OKAY' response 
	        end                   // work error responses in future
	      else
	        begin
	          if (S_AXI_BREADY && axi_bvalid) 
	            //check if bready is asserted while bvalid is high) 
	            //(there is a possibility that bready is always asserted high)   
	            begin
	              axi_bvalid <= 1'b0; 
	            end  
	        end
	    end
	end   

	// Implement axi_arready generation
	// axi_arready is asserted for one S_AXI_ACLK clock cycle when
	// S_AXI_ARVALID is asserted. axi_awready is 
	// de-asserted when reset (active low) is asserted. 
	// The read address is also latched when S_AXI_ARVALID is 
	// asserted. axi_araddr is reset to zero on reset assertion.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_arready <= 1'b0;
	      axi_araddr  <= 32'b0;
	    end 
	  else
	    begin    
	      if (~axi_arready && S_AXI_ARVALID)
	        begin
	          // indicates that the slave has acceped the valid read address
	          axi_arready <= 1'b1;
	          // Read address latching
	          axi_araddr  <= S_AXI_ARADDR;
	        end
	      else
	        begin
	          axi_arready <= 1'b0;
	        end
	    end 
	end       

	// Implement axi_arvalid generation
	// axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both 
	// S_AXI_ARVALID and axi_arready are asserted. The slave registers 
	// data are available on the axi_rdata bus at this instance. The 
	// assertion of axi_rvalid marks the validity of read data on the 
	// bus and axi_rresp indicates the status of read transaction.axi_rvalid 
	// is deasserted on reset (active low). axi_rresp and axi_rdata are 
	// cleared to zero on reset (active low).  
	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_rvalid <= 0;
	      axi_rresp  <= 0;
	    end 
	  else
	    begin    
	      if (axi_arready && S_AXI_ARVALID && ~axi_rvalid)
	        begin
	          // Valid read data is available at the read data bus
	          axi_rvalid <= 1'b1;
	          axi_rresp  <= 2'b0; // 'OKAY' response
	        end   
	      else if (axi_rvalid && S_AXI_RREADY)
	        begin
	          // Read data is accepted by the master
	          axi_rvalid <= 1'b0;
	        end                
	    end
	end    

	// Implement memory mapped register select and read logic generation
	// Slave register read enable is asserted when valid address is available
	// and the slave is ready to accept the read address.
	assign slv_reg_rden = axi_arready & S_AXI_ARVALID & ~axi_rvalid;
	always @(*)
	begin
	      // Address decoding for reading registers
	      case ( axi_araddr[ADDR_LSB+OPT_MEM_ADDR_BITS:ADDR_LSB] )
	        2'h0   : reg_data_out <= slv_reg0;
	        2'h1   : reg_data_out <= slv_reg1;
	        2'h2   : reg_data_out <= slv_reg2;
	        2'h3   : reg_data_out <= slv_reg3;
	        default : reg_data_out <= 0;
	      endcase
	end

	// Output register or memory read data
	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_rdata  <= 0;
	    end 
	  else
	    begin    
	      // When there is a valid read address (S_AXI_ARVALID) with 
	      // acceptance of read address by the slave (axi_arready), 
	      // output the read dada 
	      if (slv_reg_rden)
	        begin
	          axi_rdata <= reg_data_out;     // register read data
	        end   
	    end
	end    

	// Add user logic here
	wire [C_S_AXI_DATA_WIDTH-1:0] slv_wire2;
    wire [C_S_AXI_DATA_WIDTH-1:0] slv_wire3;
    always@(posedge S_AXI_ACLK)
    begin
        slv_reg2 <= slv_wire2;
        slv_reg3 <= slv_wire3;
    end
    
    assign slv_wire3[31:9] = 23'b0;

    huffman_tree huffman_tree_inst(
        S_AXI_ACLK,
        slv_reg0[0],
        slv_reg0[8:1],
        slv_reg1[31:0],
        slv_reg0[16:9],
        slv_wire2[31:0],
        slv_wire3[8],
        slv_wire3[7:0]
    );

	// User logic ends

	endmodule

// TREE MODULE
`define HUFF_INIT           3'b001
`define HUFF_LOAD           3'b010
`define HUFF_BUILD_TREE     3'b011
`define HUFF_MAKE_CODES     3'b100
`define HUFF_SEND_DATA      3'b101

`define INIT_STATE_1        1'b0
`define INIT_STATE_2        1'b1

`define SORT_S1             1'b0
`define SORT_S2             1'b1

`define BUILDING_TREE_S1    1'b0
`define BUILDING_TREE_S2    1'b1


`define MAKE_TREE_S1        2'b00
`define MAKE_TREE_S2        2'b01
`define MAKE_TREE_S3        2'b10

`define SEARCH_S1           1'b0
`define SEARCH_S2           1'b1

module huffman_tree(
    input wire clock,
    input wire data_enable,
    input wire [7:0] symbol_in,
    input wire [31:0] probability_in,
    input wire [7:0] symbols_length,
    output reg data_ready,
    output reg [31:0] code_out,
    output reg [7:0] code_out_length
    );
    
    parameter max_symbols_amount = 50;
    /// main variables
    reg [2:0] state = `HUFF_INIT;
    reg init_state = `INIT_STATE_1;
    reg sorting_state = `SORT_S1;
    reg building_tree_state = `BUILDING_TREE_S1;
    reg [1:0] making_tree_state = `MAKE_TREE_S1;
    reg searching_state = `SEARCH_S1;
    reg [7:0] symbols_tab[0:max_symbols_amount];
    reg [31:0] probabilities_tab[0:max_symbols_amount];
    reg [63:0] code_tab[0:max_symbols_amount];
    reg [7:0] code_length_tab[0:max_symbols_amount];
    
    parameter tree_max_length = (max_symbols_amount * 2) - 1;
    reg [31:0] ID [0:tree_max_length];
    reg [31:0] P [0:tree_max_length];
    reg [31:0] Link [0:tree_max_length];
    integer tree_tab_length;
    reg [7:0] symbols_int_length = 8'h0;
    
    /// loop variables
    integer i = 64'h0;
    integer k = 64'h0;
    integer j = 64'h0;
    integer it = 64'h0;
    integer counter = 64'h0;
    integer temp = 64'h0;
    integer temp1 = 64'h0;
    integer temp2 = 64'h0;
    
    // variables for tree
    integer code_it = 64'h0;
    integer symbol_it = 64'h0;
    integer symbol = 64'h0;
    integer position = 64'h0;
    integer current_symbol = 64'h0;
    integer last_symbol = 64'h0;
    
    always @(posedge clock) begin
        case(state)
            `HUFF_INIT: begin
                counter = 0;
                symbols_int_length = symbols_length;
                tree_tab_length = (symbols_int_length * 2) - 1;
                j = 0;
                i = 0;
                
                state = `HUFF_LOAD;
            end
            
            `HUFF_LOAD: begin
                /////////////////// load data ////////////////////////
                if(data_enable) begin
                    symbols_tab[j] = symbol_in;
                    probabilities_tab[j] = probability_in;
                    code_length_tab[j] = 0'b0;
                    //$display("j = ", j);
                    if(j == symbols_int_length) begin
                        if(i < symbols_int_length) begin
                            ID[i] = symbols_tab[i];
                            P[i] = probabilities_tab[i];
                            i = i+1;
                        end
                        else begin
                            j = 0;
                            i = 0;
                            it = 0;
                            state = `HUFF_BUILD_TREE;
                        end
                    end
                    else begin
                        j = j + 1;
                    end
                end
            end
            
            `HUFF_BUILD_TREE: begin
            /// tworzenie drzewa
            /// 1. posortuj tablice
            /// 2. znajdz dwa symbole z najmniejszymi prawdopodobinstwami - pierwszy i drugi element z tablicy
            /// 3. w tablicy Id na kolejnym pustym miejscu [length+id] wstaw nowy indeks - moze byc dowolna liczba, nieostotne jaka
            /// 4. w tablic P na tym samym miejsciu [length+id] wstaw sume prawdopodbienstw dwoch znalezionych najmniejszych prawdopodbienstw
            /// 5. w tablicy P na miejscu pierwszego z znalezionych najmniejszych prawdopodobienstw [counter] wstaw 0 a na drugim miejscu [counter+1] wstaw 1 (w przypadku tego programu jest to -1 - sprawa do roziwazania)
            /// 6. w tablicy Link na miejscach znalezionych najmniejszych prawdopodbienstw Link[counter] & Link[counter+1] wartosc nowo utworzonego Id[length+id]
            /// powtarzaj kroki 1- 6 zwiekszajac iterator tablicy o 1 oraz iterator counter o 2 az przeleci liczba dlugosci symboli - 1
            /// osiagniete tablice powinny miec przykladowy format:
            /// Id:   | A | B | C | D | E | 5 | 6 | 7 | 8  |
            /// P:    | 0 | 1 | 0 | 1 | 0 | 1 | 0 | 1 | 10 |
            /// Link  | 5 | 5 | 6 | 6 | 7 | 7 | 8 | 8 | 0  |
                    case(building_tree_state)
                    `BUILDING_TREE_S1: begin
                     ///////////////////// sorting ////////////////////////////
                         case(sorting_state)
                            `SORT_S1: begin
                                if(P[j] >= P[j+1] && P[j] > 1) begin
                                    temp = P[j];
                                    temp1 = ID[j];
                                    temp2 = Link[j];
                                    P[j] = P[j+1];
                                    ID[j] = ID[j+1];
                                    Link[j] = Link[j+1];
                                    P[j+1] = temp;
                                    ID[j+1] = temp1;
                                    Link[j+1] = temp2;
                                end
                                if(j == symbols_length + it) begin
                                    sorting_state = `SORT_S2;
                                end
                                else begin
                                    j = j + 1;
                                end
                            end
                            `SORT_S2: begin
                                if(i < symbols_length + it) begin
                                    i = i + 1;
                                    j = 0;
                                    sorting_state = `SORT_S1; 
                                end
                                else begin
                                    building_tree_state = `BUILDING_TREE_S2;
                                end
                            end
                         endcase
                    end
                    `BUILDING_TREE_S2: begin
                    //////////////////// Build a tree /////////////////////////
                        ID[symbols_length+it] = symbols_length*10+it+1;                 /// (3)
                        P[symbols_length+it] = P[counter] + P[counter+1];               /// (4)
                        P[counter] = 0;                                                 /// (5)
                        P[counter+1] = 1;                                               /// (5)
                        Link[counter] = ID[symbols_length+it];                          /// (6)
                        Link[counter+1] = ID[symbols_length+it];                        /// (6)
                        counter = counter + 2;                    
                     /////////////////// change a main state //////////////////
                        if(it < symbols_length)begin
                           building_tree_state = `BUILDING_TREE_S1;
                            it = it + 1;
                        end
                        else begin
                            state = `HUFF_MAKE_CODES;
                            last_symbol = ID[tree_tab_length - 1];
                            i = 0;
                            j = 0;
                        end
                    end
                endcase
            end
            
            `HUFF_MAKE_CODES: begin
            /// przechodzenie drzewa
            /// algorytm
            /// potrzebujemy dwa iteratory
            /// jeden do iteracji kodow - za jeden przebieg petli da sie zakodowac jeden symbol
            /// drugi iterator do przechodzenia tablicy It
            /// 1. zapisz do zmiennej tymczasowej symbol wartosc kolejnego symbolu z tablicy symbolow
            /// 2. zapisz do zmiennej current_symbol wartosc obecnego symbolu zmienna ta bedzie wykorzystywana do kolejnych iteracji
            /// 3. oblicz pozycje wybranego symbolu w tablicy Id - algorytm w funkcji na gorze
            /// 4. iezeli P[obliczonej pozycji] jest rowny 0 przesun bitowo tablice kodow o 1 (mnozymy razy 2)i dodaj 0
            /// 5. jezeli P[obliczonej pozycji] jest rowny 1(tutaj chwilowo -1) przesun bitowo tablice kodow o 1(mnozymy razy 2) i dodaj 1
            /// 6. zwieksz dlugosc danego symbolu o 1
            /// 7. zamien obecny symbol na ten z tablicy Link z obliczonej pozycji
            /// 8. jesli uzyskany nowy symbol nie jest rowny ostatniemu z drzewa z tablicy Id[ilosc_symboli*2-1] powtorz kroki 3 - 7
            /// 9. zwieksz iterator symbolowy dopoki nie przeleci wszystkich symboli - ilosc_symboli
                case(making_tree_state)
                    `MAKE_TREE_S1: begin
                        if(i < symbols_length) begin
                            symbol = symbols_tab[symbol_it];
                            current_symbol = symbol;
                            code_tab[symbol_it] = 0;
                            making_tree_state = `MAKE_TREE_S2;
                        end 
                        else begin
                            data_ready = 1'b0;
                           state = `HUFF_SEND_DATA;
                        end 
                    end
                    
                    `MAKE_TREE_S2: begin
                        if(current_symbol != last_symbol)begin
                         position = 0;
                         making_tree_state = `MAKE_TREE_S3;
                        end
                        else begin
                            symbol_it = symbol_it + 1;
                            i = i+1;
                            making_tree_state = `MAKE_TREE_S1;
                        end
                    end
                    
                    `MAKE_TREE_S3: begin
                        if(current_symbol != ID[position])begin
                            position = position + 1;
                        end
                        else begin
                            if(P[position] == 0)begin
                                code_tab[symbol_it] = code_tab[symbol_it] * 2;
                            end
                            else if(P[position] == 1) begin
                                code_tab[symbol_it] = code_tab[symbol_it] * 2;
                                code_tab[symbol_it] = code_tab[symbol_it] + 1;
                            end
                            code_length_tab[symbol_it] = code_length_tab[symbol_it] + 1;
                            current_symbol = Link[position];
                            making_tree_state = `MAKE_TREE_S2;
                        end
                    end
               endcase
            end
            
            `HUFF_SEND_DATA: begin
                if(i < symbols_length) begin
                    code_out = code_tab[i];
                    code_out_length = code_length_tab[i];
                    i = i + 1;
                end
            end
        endcase
    end
endmodule 
