`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.05.2019 10:56:03
// Design Name: 
// Module Name: huffman_codec_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module huffman_codec_tb;

reg clock;
reg data_enable;
reg [7:0] data_in;
reg [7:0] data_count;
wire coded_out;
wire [7:0] decoded_out;
wire coded_data_ready;
wire decoded_data_ready;

integer i = 32'h0;

reg [7:0] data_to_input [0:49] = {32, 44, 46, 59, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 82, 83, 84, 85, 87, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 114, 115, 116, 117, 119, 121, 122};

huffman_codec codec(
    .clock(clock),
    .data_enable(data_enable),
    .data_in(data_in),
    .data_count(data_count),
    .coded_out(coded_out),
    .decoded_out(decoded_out),
    .coded_out_data_ready(coded_data_ready),
    .decoded_out_data_ready(decoded_data_ready)
    );
    
    initial begin
        forever #10 clock = ~clock;
    end
    
    initial begin
        clock = 0;
        data_in = 0;
        data_enable = 0;
        data_count = 50;
        
         data_enable = 1;
         
         for(i =0; i < 50; i = i + 1) begin
            data_in = data_to_input[i];
            #20;
         end
        
        #1000000; //delay 2100ps to finish testing
        $finish; // inform that the test was finished
    end

endmodule
