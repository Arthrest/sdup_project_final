`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: AGH University of Science and Technology
// Engineer: Arkadiusz Balys, Kamil Kasperczyk
// 
// Create Date: 20.04.2019 20:26:41
// Design Name: Huffman algorithm for subject Systemy Dedykowane w uk�adach programowalnych 
// Module Name: huffman_tb
// Project Name: Huffman Project SDUP 2019
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: huffman_core.v
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
/// TEST BENCH ///


module huffman_tb;

    parameter polish_symbols_amount = 50;

/// tabs of symbols and probabilities
 reg [7:0] symbols [0:polish_symbols_amount - 1] = {32, 44, 46, 59, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 82, 83, 84, 85, 87, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 114, 115, 116, 117, 119, 121, 122};
 reg [31:0] probabilities [0:polish_symbols_amount - 1] = {1720, 90, 90, 50, 400, 65, 190, 150, 345, 5, 50, 50, 350, 95, 135, 155, 120, 235, 355, 120, 175, 190, 120, 90, 180, 160, 290, 400, 65, 190, 150, 345, 5, 50, 50, 350, 95, 135, 155, 120, 235, 355, 120, 175, 190, 120, 90, 180, 160, 290};
 //reg [31:0] probabilities [0:26] = {172,9,9,5,80,13,38,30,69,1,10,10,70,19,27,31,24,47,71,24,35,38,24,18,36,32,58};
  //  reg [7:0] symbols [0:26] = {32, 44, 46, 59, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 82, 83, 84, 85, 87, 89, 90};
// Inputs
    reg clock;
    reg [7:0] data_in;
    reg data_enable;
    reg [7:0] data_length;
    reg [31:0] prob_in;

// Outputs
    wire [63:0] code_out;
    wire [7:0] code_out_length;

// Variables
integer i = 64'h0;
  
    huffman_tree tree(
        .clock(clock),
        .data_enable(data_enable),
        .symbol_in(data_in),
        .probability_in(prob_in),
        .symbols_length(data_length),
        .code_out(code_out),
        .code_out_length(code_out_length)
    );
    
// set clock in period 10ps -> #10
    initial begin
    forever #10 clock = ~clock;
    end
        
// initialize inputs with enabling data in each 20ps -> #20
    initial begin
        clock = 0;
        data_in = 0;
        data_enable = 0;
        data_length = 50;
        
        #100;
        /// ladowanie danych do budowania drzewa 
        for(i = 0; i < 50; i = i + 1) begin
            data_enable = 1;
            data_in = symbols[i];
            prob_in = probabilities[i];
            #20;
        end
        
       
        data_enable = 0;
        #20 data_enable = 1;
        
        #100000; //delay 2100ps to finish testing
        $finish; // inform that the test was finished
    end
  
      
endmodule