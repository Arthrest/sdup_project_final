`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 24.04.2019 08:33:56
// Design Name: 
// Module Name: sort
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//`define INIT        2'b01
//`define PROCESS     2'b10
//`define SEND_DATA   2'b11

//module sort(
//    input wire clock,
//    input wire [31:0] data_in,
//    input wire data_enable,
//    input wire [31:0] data_length,
//    output reg [31:0] data_out
//    );

//parameter max_length_data = 2**32;
    
//reg [1:0] state = `INIT;
//reg [31:0] data_in_table [max_length_data:0];
//integer i = 32'h0;

//always @(posedge clock) begin

//case(state)
/////////////////////////////////////////////////////////////////////////////////////////
////              Case INIT - initialize tables and variables
///////////////////////////////////////////////////////////////////////////////////////// 
//    `INIT: begin
//        for(i = 0; i < max_length_data; i = i + 1) begin
//            data_in_table[i] = 'bz;
//        end
//        state = `GET_DATA;
//    end
/////////////////////////////////////////////////////////////////////////////////////////
////              Case GET_DATA - get data from input
/////////////////////////////////////////////////////////////////////////////////////////     
//    `GET_DATA: begin
//        if(data_enable) begin
//            data_in_table[i] = data_in;
//        end
//        i = i + 1; 
//        if(i == data_length) begin
//            i = 0;
//            state = `PROCESS;
//        end
//    end
/////////////////////////////////////////////////////////////////////////////////////////
////              Case PROCESS - sort the input table
///////////////////////////////////////////////////////////////////////////////////////// 
//endcase
//end
//endmodule



//module find(
//    input clock,
//    input wire [7:0] data_in,
//    input wire data_enable,
//    input wire [7:0] data_length,
//    output reg [7:0] data_out
//    );
//endmodule
