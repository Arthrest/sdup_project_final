`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.05.2019 20:46:25
// Design Name: 
// Module Name: huffman_coder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`define CODER_INIT       2'b00
`define DATA_LOADING     2'b01
`define CODING           2'b10
`define SENDING_DATA     2'b11

`define CODING_STATE_1 2'b00
`define CODING_STATE_2 2'b01
`define CODING_STATE_3 2'b10

module huffman_coder(
    input wire clock,
    input wire coder_data_enable,
    input wire [7:0] input_data_stream,
    input wire [7:0] input_data_stream_length,
    input wire [31:0] code_list_stream,
    input wire [7:0] codes_length_stream,
    input wire [7:0] symbols_stream,
    input wire [7:0] symbols_amount,
    output reg coder_data_ready,
    output reg coded_bit_stream
    );
    
    integer input_data_length = 32'h0;
    integer symbols_amount_xd = 32'h0;
    reg [1:0] state = `CODER_INIT;
    reg [1:0] coding_state = `CODING_STATE_1;

    reg [31:0] code_list [0:99];
    reg [7:0] codes_lengths [0:99];
    reg [7:0] symbols [0:99];
    reg [7:0] input_data [0:999];
    reg code_bit_tab [0:999];
  
    integer init_data_counter = 32'h0;
    integer character_stream_counter = 32'h0;
    integer coder_iterator = 32'h0;
    integer i = 32'h0;
    integer l = 32'h0;
    integer j = 32'h0;
    integer k = 32'h0;
    
    integer position = 32'h0;
  
    always @(posedge clock) begin
        case(state)
            `CODER_INIT: begin
                if(coder_data_enable)begin
                    input_data_length = input_data_stream_length;
                    symbols_amount_xd = symbols_amount;
                    code_list[init_data_counter] = code_list_stream;
                    codes_lengths[init_data_counter] = codes_length_stream;
                    symbols[init_data_counter] = symbols_stream;
                    init_data_counter = init_data_counter + 1;
                    if(init_data_counter == symbols_amount_xd) begin
                        state = `DATA_LOADING;
                        coder_iterator = 0;
                        k = 0;
                       // $display("Finished loading init data");         
                    end
                end
            end
            `DATA_LOADING: begin
            if(coder_data_enable)begin
                input_data[character_stream_counter] = input_data_stream;
                character_stream_counter = character_stream_counter + 1;
            end
                if(character_stream_counter == input_data_length) begin
                    state = `CODING;
                    //$display("Finished loading input data");
//                    for(i=0;i<input_data_stream_length;i=i+1)$display("input characters: ", input_data[i]); 
                end
            end
            `CODING: begin
                case(coding_state)
                    `CODING_STATE_1: begin
                        //$display("STATE 1");
                        j = 0;
                        if(coder_iterator < input_data_length) begin
                            position = 0;
                            coding_state = `CODING_STATE_2;
                        end
                        else begin
                            state = `SENDING_DATA;
                            j = 0;
                        end
                    end
                    `CODING_STATE_2: begin
                        //$display("STATE 2");
                        if((symbols[position] != input_data[coder_iterator])) begin
                            position = position + 1;
                        end
                        else begin
                           coding_state = `CODING_STATE_3; 
                           l = 1;
                        end
                    end
                    `CODING_STATE_3: begin
                        //$display("STATE 3");
                        if(j < codes_lengths[position]) begin
                            if((code_list[position] & l) != 0) begin
                                //$display("1");
                                code_bit_tab[k] = 1;
                                k = k + 1;
                            end
                            else begin
                               // $display("0");
                                code_bit_tab[k] = 0;
                                k = k + 1;
                            end
                            // multiply by 2
                            l = l << 1;
                            j = j + 1;
                        end
                        else begin
                            coder_iterator = coder_iterator + 1;
                            coding_state = `CODING_STATE_1;
                        end
                    end
                endcase
             end           
            `SENDING_DATA: begin
                if(j < k) begin
                    //$display("SENDING");
                    coder_data_ready = 1;
                    coded_bit_stream = code_bit_tab[j];
                    j = j + 1;
                end
                else begin
                    coder_data_ready = 0;
                end
            end
        endcase
    end
endmodule
